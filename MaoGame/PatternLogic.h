#pragma once



#include "Rule.h"



/*
 Currently stores a list of conditions for each Effect.
 
 This is done like so:

 Conditions:
 [isNumber][parity][suit][rank]

*/
class PatternLogic
{
public:
	PatternLogic();
	~PatternLogic();

	void addPair(Condition c);

	//std::vector<EffectDomain> effects;
	
private:

	int numRules;
};

