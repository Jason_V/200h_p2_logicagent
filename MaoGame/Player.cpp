#include "Player.h"
#include "Rand.h"


Player::Player()
{
}


Player::~Player()
{
}

void Player::draw(int num)
{
	for (int i = 0; i < num; i++) {
		hand[i] = Rand::newCard();
	}
}

Card Player::playCard(int ruleCount)
{
	int index = Rand::newInt(7);
	Card c = hand[index];
	hand[index] = Rand::newCard();
	return c;
}

void Player::printMetaData(int ruleCount)
{
	
}

void Player::test(ValuedCat condition)
{
	auto effect = history.begin();

	std::vector<int> temp;

	while (effect != history.end()) {
		effect->second.makeSets();
		temp[temp.size()] = effect->second.check(condition);
		effect++;
	}

	std::cout << "Per effect, the number of historical events not explained by a rule for the category: " << std::endl;
	for (int i = 0; i < temp.size(); i++) {
		std::cout << temp[i] << std::endl;
	}

}

void Player::addResult(Card played, int result)
{
	if (history.count(result) == 0) {
		EffectConditions e;
		e.init(result, played);
		history[result] = e;
	}
	else {
		history[result].addTrigger(played);
	}
}

