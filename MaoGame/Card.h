#pragma once

#include <string>
#include <iostream>

class Card {
public:
	int cats[4];

	void setSuit(int suit) {
		cats[2] = suit;
	}

	void setRank(int rank) {
		cats[3] = rank;
		if (rank < 11) {
			cats[0] = 1;
			cats[1] = rank % 2 == 0;
		}
		else {
			//is a face card; no parity
			cats[1] = -1;
			cats[0] = 0;
		}
	}

	void print() {
		std::cout << "(" << cats[0];
		for (int i = 1; i < 4; i++) {
			std::cout << ", " << cats[i];
		}
		std::cout << ")" << std::endl;
	}
};