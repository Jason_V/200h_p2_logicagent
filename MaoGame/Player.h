#pragma once



#include "Card.h"
#include "EffectConditions.h"

class Player
{
public:
	Player();
	~Player();

	void draw(int num);

	Card playCard(int ruleCount);

	void printMetaData(int ruleCount);

	//-1 is no penalty
	void addResult(Card played, int result);

private:
	std::map<int, EffectConditions> history;
	std::array<Card, 7> hand;

	void test(ValuedCat condition);
};

