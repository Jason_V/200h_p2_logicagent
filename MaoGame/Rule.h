#pragma once
#include "Card.h"
#include <string>

struct Condition {
	int category;
	int value;
	void print() {
		std::cout << "category: " << category << " , value: " << value << std::endl;
	}
};

struct Rule {

	int penalty;
	Condition condition;

	bool matches(Card card) {
		return card.cats[condition.category] == condition.value;
	}

	bool penalizes(Card card) {
		if (penalty != 0 && penalty != 1) {
			return card.cats[condition.category] == condition.value;
		}
		return false;
	}

	void print() {
		std::cout << "Penalty: " << penalty << ", Condition: ";
		condition.print();
	}

};