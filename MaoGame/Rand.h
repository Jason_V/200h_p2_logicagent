#pragma once

#include <stdlib.h> 

#include "Rule.h"


struct Rand {

	static Condition newCondition() {
		int temp[] = { 2 , 2 , 4 , 14 };
		Condition c;
		c.category = std::rand() % 4;
		c.value = std::rand() % temp[c.category];

		return c;
	}

	static Rule newRule() {

		Rule r;
		r.condition = newCondition();
		r.penalty = std::rand() % 6;

		return r;
	}

	static Card newCard() {
		Card c;
		c.setRank(std::rand() % 14);
		c.setSuit(std::rand() % 4);
		return c;
	}

	static int newInt(int bound) {
		return std::rand() % bound;
	}

};