# README #

This project serves as a logic-based rule guesser for an AI playing the card game Mao.
Currently, given a card to test and knowledge of the game state, it prints out per-effect how likely that card is to trigger that effect.
Effects:
0-continue/allow, 1-block, 2-skipTo, 3-mustSay, 4-reverse